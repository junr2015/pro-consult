package com.junr.myapplication;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.junr.myapplication.rest.Issue;
import com.junr.myapplication.rest.RESTyoutrack;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;
import rx.util.functions.Action1;


public class FragmentIssueInfo extends Fragment {
    private static final Set<String> HIDDEN_PROPS = new HashSet<String>();
    static {
        HIDDEN_PROPS.add("id");
        HIDDEN_PROPS.add("summary");
        HIDDEN_PROPS.add("description");
        HIDDEN_PROPS.add("numberInProject");
        HIDDEN_PROPS.add("projectShortName");
    }
    public static final String ARG_ID = "id";

    private String id;

    public FragmentIssueInfo() {
        setArguments(new Bundle());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getIssueById();
    }

    @Override
    public void onResume() {
        super.onResume();
        getIssueById();
    }

    private void getIssueById(){
        if (getArguments() != null) {
            id = getArguments().getString(ARG_ID);
        }
        getIssueObs();
    }

    public void getIssueObs() {
        rx.Observable.create(new rx.Observable.OnSubscribeFunc<Issue>() {
            @Override
            public Subscription onSubscribe(rx.Observer<? super Issue> observer) {
                try {
                    final Uri issueUri = MyApplication.app().rest().getIssueUri(id);
                    Issue issue = MyApplication.app().rest().getIssue(issueUri.toString());
                    observer.onNext(issue);
                    observer.onCompleted();
                } catch (Exception e) {
                    observer.onError(e);
                }
                return Subscriptions.empty();
            }
        }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Issue>() {
                    @Override
                    public void call(Issue issue) {
                        TextView summary = (TextView) getActivity().findViewById(R.id.summary);
                        summary.setText(issue.issue().get(RESTyoutrack.FIELD_ISSUE_INFO_SUMMARY));

                        String description = issue.issue().get(RESTyoutrack.FIELD_ISSUE_INFO_DESCRIPTION);
                        if (description != null && description.length() > 0) {
                            TextView descriptionView = (TextView) getActivity().findViewById(R.id.description);
                            descriptionView.setText(description);
                        }

                        ListView propertiesList = (ListView) getActivity().findViewById(R.id.issue_properties);
                        List<String> issueData = new ArrayList<String>();
                        for (String fieldName: issue.issue().keySet()) {
                            String value = issue.issue().get(fieldName);
                            if (!HIDDEN_PROPS.contains(fieldName) && value != null && value.length() > 0) {

                                if (fieldName.equalsIgnoreCase("updated")
                                        || fieldName.equalsIgnoreCase("created")) {
                                    SimpleDateFormat sdf = new SimpleDateFormat("d MMM yyyy, HH:mm");
                                    value = sdf.format(new Date(Long.parseLong(value)));
                                }
                                issueData.add(fieldName + ": " + value);
                            }
                        }
                        propertiesList.setAdapter(
                                new ArrayAdapter<String>(
                                        getActivity(),
                                        R.layout.issue_field, issueData
                                )
                        );

                        try {
                            // comments
                            TextView textViewComments = (TextView) getActivity()
                                    .findViewById(R.id.issue_comments);
                            {
                                StringBuilder sb = new StringBuilder();
                                sb.append("Comments:\n");
                                String strComments = "";
                                Map<String, Issue.Comment> comments = issue.comments();
                                ArrayList<String> keys = new ArrayList<String>(comments.keySet());

                                Collections.sort(keys);

                                for (String id : keys) {
                                    Issue.Comment comment = comments.get(id);
                                    sb.append(comment.authorFullName());
                                    sb.append(" : ");
                                    sb.append(comment.text());
                                    sb.append("\n");
                                }
                                strComments = sb.toString();

                                if ("Comments:\n".equalsIgnoreCase(strComments)) {
                                    strComments = "No comments.";
                                }
                                textViewComments.setText(strComments);
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable error) {
                        Toast.makeText(getActivity(),
                                error.getMessage(),
                                Toast.LENGTH_SHORT)
                                .show();
                    }
                });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_issue_info, container, false);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
