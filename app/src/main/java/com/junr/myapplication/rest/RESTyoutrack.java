package com.junr.myapplication.rest;

import android.net.Uri;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DefaultHandler2;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class RESTyoutrack {
    private static final String HOST =     "http://junr.myjetbrains.com/youtrack";
    private static final String LOGIN =    "root";
    private static final String PASSWORD = "160690";
    public  static final String PROJECT =  "MyProj";

    private static final String LOGIN_PATH = "%s/rest/user/login?login=%s&password=%s";
    private static final String ISSUES_PATH = "%s/rest/issue/byproject/%s?max=999"; //?filter=%s&after=%d";
    private static final String ISSUE_PATH = "%s/rest/issue/%s";
    private static final String CREATE_ISSUE = "%s/rest/issue?project=" + PROJECT + "&summary=%s&description=%s";

    public static final String FIELD_ISSUE_ID = "id";
    public static final String FIELD_ISSUE_STATE = "state";
    public static final String FIELD_ISSUE_PRIORITY = "Priority";
    public enum PRIORITY_VALUES {
          Minor
        , Major
        , Critical
        , Showstopper
    }
    public static final String FIELD_ISSUE_INFO_SUMMARY = "summary";
    public static final String FIELD_ISSUE_INFO_DESCRIPTION = "description";

    private HttpClient httpClient;

    public RESTyoutrack() {
        httpClient = new DefaultHttpClient();
    }

    public HttpEntity login() throws RequestFailedException {
        try {
            String uri = String.format(LOGIN_PATH, HOST, quote(LOGIN), quote(PASSWORD));

            HttpPost post = new HttpPost(uri);
            HttpResponse response = httpClient.execute(post);
            HttpEntity entity = response.getEntity();

            assertStatus(response);
            post.abort();
            return entity;
        } catch (IOException e) {
            throw new RequestFailedException(e);
        }
    }

    public HttpEntity createIssue(String summary, String description) throws RequestFailedException {
        try {
            String uri = String.format(CREATE_ISSUE, HOST, quote(summary), quote(description));

            HttpPut put = new HttpPut(uri);
            HttpResponse response = httpClient.execute(put);
            HttpEntity entity = response.getEntity();

            assertStatus(response);
            put.abort();
            return entity;
        } catch (IOException e) {
            throw new RequestFailedException(e);
        }
    }

    public Uri getIssueUri(String issueId) {
        return Uri.parse(String.format(ISSUE_PATH, HOST, quote(issueId)));
    }

    public Issue getIssue(String issueUri) throws RequestFailedException {
        try {
            HttpGet get = new HttpGet(issueUri);
            HttpResponse response = httpClient.execute(get);
            assertStatus(response);

            HttpEntity responseEnt = response.getEntity();

            final Issue issue = new Issue();

            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
            parser.parse(responseEnt.getContent(), new DefaultHandler2() {
                private String fieldName;
                private StringBuilder multiValue;
                private StringBuilder value;

                @Override
                public void startElement(String uri, String localName, String qName, Attributes attributes)
                        throws SAXException {
                    if ("issue".equalsIgnoreCase(localName)) {
                        String issueId = attributes.getValue(FIELD_ISSUE_ID);
                        issue.put(FIELD_ISSUE_ID, issueId);
                    }
                    if ("field".equalsIgnoreCase(localName)) {
                        fieldName = attributes.getValue("name");
                        multiValue = new StringBuilder();
                    }
                    if ("value".equalsIgnoreCase(localName)) {
                        value = new StringBuilder();
                    }
                    if ("comment".equalsIgnoreCase(localName)) {
                        String id = attributes.getValue("id");
                        String authorFullName = attributes.getValue("authorFullName");
                        String text = attributes.getValue("text");
                        issue.putComment(id, authorFullName, text);
                    }
                }

                @Override
                public void endElement(String uri, String localName, String qName) throws SAXException {
                    try {
                        if ("field".equalsIgnoreCase(localName)) {
                            issue.put(fieldName, multiValue.toString());
                            fieldName = null;
                        }
                        if ("value".equalsIgnoreCase(localName)) {
                            if (multiValue.length() > 0) {
                                multiValue.append(", ");
                            }
                            multiValue.append(value);
                            value = null;
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void characters(char[] ch, int start, int length) throws SAXException {
                    try {
                        value.append(ch, start, length);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            });
            responseEnt.consumeContent(); // lol
            return issue;
        } catch (Exception e) {
            throw new RequestFailedException(e);
        }
    }

    public IssueItems getIssues()
            throws RequestFailedException {
        IssueItems issuesItems = loadIssues();
        return issuesItems;
    }

    private IssueItems loadIssues()
            throws RequestFailedException {
        IssueItems issueItems = new IssueItems(null);
        final ArrayList<Map<String, Object>> issues = new ArrayList<Map<String, Object>>();
        try {
            String uri = String.format(ISSUES_PATH, HOST,  quote(PROJECT));

            HttpGet get = new HttpGet(uri);
            HttpResponse response = httpClient.execute(get);
            assertStatus(response);

            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
            parser.parse(response.getEntity().getContent(), new DefaultHandler2() {

                @Override
                public void startElement(String uri, String localName, String qName, Attributes attributes)
                        throws SAXException {
                    if ("issue".equalsIgnoreCase(localName)) {
                        Map<String, Object> issue = new HashMap<String, Object>();
                        for (int i = 0; i < attributes.getLength(); i++) {
                            String name = attributes.getLocalName(i);
                            String value = attributes.getValue(i);
                            issue.put(name, value);
                        }
                        issues.add(issue);
                    }
                }
            });
            issueItems = new IssueItems(issues);
            response.getEntity().consumeContent(); // lol
        } catch (Exception e) {
            throw new RequestFailedException(e);
        }
        return issueItems;
    }

    public void destroy() {
        httpClient.getConnectionManager().shutdown();
    }

    private void assertStatus(HttpResponse response) throws RequestFailedException {
        int status = response.getStatusLine().getStatusCode();

        if (status == 401) {
            throw new RequestFailedException(String.valueOf(status));
        }

        if (status >= 300) {
            throw new RequestFailedException(status + ": " + response.getStatusLine().getReasonPhrase());
        }
    }

    private String quote(String value) {
        try {
            return URLEncoder.encode(value, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
