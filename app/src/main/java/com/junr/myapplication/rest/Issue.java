package com.junr.myapplication.rest;

import java.util.HashMap;
import java.util.Map;

public class Issue {

    public class Comment {
        private String authorFullName;
        private String text;

        public Comment(String authorFullName, String text) {
            this.authorFullName = authorFullName;
            this.text = text;
        }

        public String authorFullName() {
            return authorFullName;
        }
        public String text() {
            return text;
        }
    }

    public Issue() {
        issue = new HashMap<>();
        comments = new HashMap<>();
    }

    private Map<String, String> issue;
    private Map<String, Comment> comments;

    public Map<String, String> issue() {
        return issue;
    }
    public void put(String key, String value) {
        issue.put(key, value);
    }

    public Map<String, Comment> comments() {
        return comments;
    }
    public void putComment(String id, String authorFullName, String text) {
        comments.put(id, new Comment(authorFullName, text));
    }
}
