package com.junr.myapplication.rest;

public class RequestFailedException extends Exception {

    public RequestFailedException(String message) {
        super(message);
    }

    public RequestFailedException(Throwable cause) {
        super(cause);
    }
}
