package com.junr.myapplication.rest;

import java.util.ArrayList;
import java.util.Map;

public class IssueItems {
    private ArrayList<Map<String, Object>> issues;

    public IssueItems(ArrayList<Map<String, Object>> issues){
        this.issues = issues;
    }

    public ArrayList<Map<String, Object>> issues(){
        if (issues == null) {
            issues = new ArrayList<Map<String, Object>>();
        }
        return issues;
    }
}
