package com.junr.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Toast;

import com.junr.myapplication.rest.IssueItems;
import com.junr.myapplication.rest.RequestFailedException;
import com.melnykov.fab.FloatingActionButton;
import com.mikepenz.iconics.typeface.FontAwesome;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import org.apache.http.HttpEntity;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;
import rx.util.functions.Action1;


public class ActivityMain extends AppCompatActivity
        implements FragmentIssues.OnFragmentIssuesListener,
                   FragmentCreateIssue.OnFragmentCreateIssueListener {

    private Drawer.Result drawerResult = null;
    private FragmentIssues fragmentIssues;
    private FragmentIssueInfo fragmentIssueInfo;
    private FragmentCreateIssue fragmentCreateIssue;

    private HttpEntity loginResponseEntity;
    private IssueItems data;

    private FloatingActionButton fabnew;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        handler = new Handler();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fabnew = (FloatingActionButton) findViewById(R.id.fabnew);
        fabnew.setVisibility(View.VISIBLE);
        fabnew.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                createIssue();
            }
        });
        initDrawer(toolbar);
        initFragments();
        loginObs();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getIssuesObs();
        refreshContainer();
    }

    private void initDrawer(Toolbar toolbar) {
        drawerResult = new Drawer()
                .withActivity(this)
                .withToolbar(toolbar)
                .withActionBarDrawerToggle(true)
                .withHeader(R.layout.drawer_header)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.drawer_item_issues)
                                .withIcon(FontAwesome.Icon.faw_home),
                        new SecondaryDrawerItem().withName(R.string.drawer_item_open_source)
                                .withIcon(FontAwesome.Icon.faw_github),
                        new DividerDrawerItem(),
                        new SecondaryDrawerItem().withName(R.string.drawer_item_contact)
                                .withIcon(FontAwesome.Icon.faw_envelope_o).withBadge("*")
                )
                .withOnDrawerListener(new Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerOpened(View drawerView) {
                        InputMethodManager inputMethodManager = (InputMethodManager)
                                ActivityMain.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(
                                ActivityMain.this.getCurrentFocus().getWindowToken(), 0);
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {
                    }
                })
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l,
                                            IDrawerItem iDrawerItem) {

                        switch (i) {
                            case 1: //  issues
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        getIssuesObs();
                                    }
                                });
                                break;
                            case 2: //  source
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(
                                        getResources().getString(R.string.activity_main_gitrepo)));
                                startActivity(browserIntent);
                                break;
                            case 4: //  contacts
                                Toast.makeText(
                                        ActivityMain.this,
                                        getResources().getString(R.string.activity_main_contacts),
                                        Toast.LENGTH_LONG
                                ).show();
                                break;
                            default:
                                break;
                        }
                    }
                })
                .build();
    }

    private void initFragments() {
        fragmentIssues = new FragmentIssues();
        fragmentIssueInfo = new FragmentIssueInfo();
        fragmentCreateIssue = new FragmentCreateIssue();
    }
    private void refreshContainer(){
        View fragCont = (View) findViewById(R.id.fragment_container);
        {
            fragCont.setVisibility(View.GONE);
            fragCont.setVisibility(View.VISIBLE);
        }
    }

    private void showIssues() {

        fragmentIssues.setIssues(getApplicationContext(), data);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragmentIssues)
                .commit();
        refreshContainer();
        fabnew.setVisibility(View.VISIBLE);
    }
    private void showIssueInfo(String id){
        fragmentIssueInfo.getArguments()
                .putString(FragmentIssueInfo.ARG_ID, id);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragmentIssueInfo)
                .addToBackStack(null)
                .commit();
        fabnew.setVisibility(View.GONE);
    }
    private void createIssue(){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragmentCreateIssue)
                .addToBackStack(null)
                .commit();
        fabnew.setVisibility(View.GONE);
    }

    private void loginObs(){
        Observable.create(new Observable.OnSubscribeFunc<HttpEntity>() {
            @Override
            public Subscription onSubscribe(Observer<? super HttpEntity> observer) {
                try {
                    loginResponseEntity = MyApplication.app().rest().login();
                    observer.onNext(loginResponseEntity);
                    observer.onCompleted();
                } catch (Exception e) {
                    observer.onError(e);
                }
                return Subscriptions.empty();
            }
        }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<HttpEntity>() {
                    @Override
                    public void call(HttpEntity response) {
                        try {
                            if (EntityUtils.toString(response, HTTP.UTF_8).trim().equalsIgnoreCase(
                                    getResources().getString(R.string.rest_login_ok))) {

                                response.consumeContent(); // lol

                                getIssuesObs();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable error) {
                        Toast.makeText(ActivityMain.this,
                                error.getMessage(),
                                Toast.LENGTH_SHORT)
                                .show();
                    }
                });
    }

    private void getIssuesObs() {
        Observable.create(new Observable.OnSubscribeFunc<IssueItems>() {
            @Override
            public Subscription onSubscribe(Observer<? super  IssueItems> observer) {
                try {
                    updateQuery();
                    observer.onNext(data);
                    observer.onCompleted();
                } catch (Exception e) {
                    if (e.getMessage().contains(
                            getResources().getString(R.string.rest_error_code_not_authorized))) {
                        loginObs(); // reLogin
                    } else {
                        observer.onError(e);
                    }
                }

                return Subscriptions.empty();
            }
        }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<IssueItems>() {
                    @Override
                    public void call(IssueItems response) {
                        if (response != null && response.issues().size() > 0) {
                            showIssues();
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable error) {
                        Toast.makeText(ActivityMain.this,
                                error.getMessage(),
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });
    }

    private void updateQuery() throws RequestFailedException {
        try {
            data = MyApplication.app().rest().getIssues();
        } catch (RequestFailedException ex) {
            throw ex;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_issues) {
            getIssuesObs();
            refreshContainer();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    @Override
    public void onBackPressed(){
        if(drawerResult.isDrawerOpen()){
            drawerResult.closeDrawer();
        } else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onFragmentIssuesItemClicked(String id) {
        showIssueInfo(id);
    }

    @Override
    public void onFragmentIssuesResumed() {
        if (fabnew != null) {
            fabnew.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void issueCreated() {
        getIssuesObs();
        refreshContainer();
    }
}
