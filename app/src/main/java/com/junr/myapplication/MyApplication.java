package com.junr.myapplication;

import android.app.Application;

import com.junr.myapplication.rest.RESTyoutrack;

public class MyApplication extends Application {
    private static MyApplication application;
    private RESTyoutrack rest;

    MyApplication(){
        rest = new RESTyoutrack();
    }

    public static MyApplication app() {
        if (application == null) {
            application = new MyApplication();
        }
        return application;
    }
    public RESTyoutrack rest() {
        return rest;
    }

    @Override
    public void onTerminate() {
        rest.destroy();
        super.onTerminate();
    }
}
