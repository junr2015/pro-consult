package com.junr.myapplication;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.TextView;

import com.junr.myapplication.rest.IssueItems;
import com.junr.myapplication.rest.RESTyoutrack;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FragmentIssues extends Fragment implements AdapterView.OnItemClickListener {
    private AbsListView mListView;
    private AdapterIssueList mAdapter;
    private OnFragmentIssuesListener onFragmentIssuesListener;

    public interface OnFragmentIssuesListener {
        void onFragmentIssuesItemClicked(String id);
        void onFragmentIssuesResumed();
    }

    public FragmentIssues() {
    }

    public void setIssues(Context context, IssueItems issues) {
        mAdapter = new AdapterIssueList(context, issues);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mAdapter == null) {
            mAdapter = new AdapterIssueList(getActivity(), new IssueItems(null));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_issues, container, false);

        mListView = (AbsListView) view.findViewById(android.R.id.list);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);

        setEmptyText(getString(R.string.fragment_issues_no_issues));
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (onFragmentIssuesListener != null) {
            onFragmentIssuesListener.onFragmentIssuesResumed();
        }

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            onFragmentIssuesListener = (OnFragmentIssuesListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onFragmentIssuesListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != onFragmentIssuesListener) {
            onFragmentIssuesListener.onFragmentIssuesItemClicked(
                    mAdapter.getItem(position).get(RESTyoutrack.FIELD_ISSUE_ID).toString() );
        }
    }

    public void setEmptyText(CharSequence emptyText) {
        View emptyView = mListView.getEmptyView();

        if (emptyView == null) {
            mListView.setEmptyView(getActivity().findViewById(android.R.id.empty));
            emptyView = mListView.getEmptyView();
        }

        if (emptyView instanceof TextView) {
            TextView emptyTV = ((TextView) emptyView);
            emptyTV.setTextColor(Color.GRAY);
            emptyTV.setText(emptyText);
        }
    }

}
