package com.junr.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.junr.myapplication.rest.RESTyoutrack;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.apache.http.HttpEntity;

import java.io.IOException;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;
import rx.util.functions.Action1;


public class FragmentCreateIssue extends Fragment {

    private MaterialEditText editSummary;
    private MaterialEditText editDescription;

    public interface OnFragmentCreateIssueListener {
        void issueCreated();
    }
    private OnFragmentCreateIssueListener onFragmentCreateIssueListener;


    public FragmentCreateIssue() {
        // Required empty public constructor
    }

    public void createIssue(final String summary, final String description) {
        rx.Observable.create(new rx.Observable.OnSubscribeFunc<HttpEntity>() {
            @Override
            public Subscription onSubscribe(rx.Observer<? super HttpEntity> observer) {
                try {
                    HttpEntity entity = MyApplication.app().rest()
                            .createIssue(summary, description);
                    observer.onNext(entity);
                    observer.onCompleted();
                } catch (Exception e) {
                    observer.onError(e);
                }

                return Subscriptions.empty();
            }
        }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<HttpEntity>() {
                    @Override
                    public void call(HttpEntity httpEntity) {
                        try {
                            httpEntity.consumeContent(); // lol
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(
                                getActivity(),
                                getActivity().getResources().getString(R.string.toast_issue_created),
                                Toast.LENGTH_SHORT).show();
                        clearForm();
                        onFragmentCreateIssueListener.issueCreated();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable error) {
                        Toast.makeText(getActivity(),
                                error.getMessage(),
                                Toast.LENGTH_SHORT)
                                .show();
                    }
                });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_create_issue, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            onFragmentCreateIssueListener = (OnFragmentCreateIssueListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onStart() {
        final MaterialEditText editProject = (MaterialEditText) getActivity()
                .findViewById(R.id.issue_project);
        editProject.setText(RESTyoutrack.PROJECT);
        editSummary = (MaterialEditText) getActivity()
                .findViewById(R.id.issue_summary);
        editDescription = (MaterialEditText) getActivity()
                .findViewById(R.id.issue_description);

        final Button btnCreate = (Button) getActivity().findViewById(R.id.btn_create_issue);
        btnCreate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                processForm();
            }
        });
        super.onStart();
    }

    private void processForm(){
        final String summary = editSummary.getText().toString();
        final String description = editDescription.getText().toString();

        if (summary.trim().length() > 0
                && description.trim().length() > 0) {
            createIssue(summary, description);
        } else {
            Toast.makeText(
                    getActivity(),
                    getActivity().getResources().getString(
                            R.string.fragment_create_issue_error_fill_all_fields),
                    Toast.LENGTH_SHORT
            ).show();
        }
    }
    private void clearForm(){
        editSummary.setText("");
        editDescription.setText("");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onFragmentCreateIssueListener = null;
    }
}
