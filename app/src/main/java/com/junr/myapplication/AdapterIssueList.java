package com.junr.myapplication;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.junr.myapplication.rest.IssueItems;
import com.junr.myapplication.rest.RESTyoutrack;

import java.util.Map;

class AdapterIssueList extends ArrayAdapter {
    private Context context;
    private Resources resources;
    private IssueItems items;
    private String[] states;

    public AdapterIssueList(Context context, IssueItems items) {
        super(context, R.layout.issue_list_item);
        this.context = context;
        this.resources = context.getResources();
        this.items = items;
        this.states = context.getResources().getStringArray(R.array.issue_states);
    }

    @Override
    public int getCount() {
        return items.issues().size();
    }

    static class ViewHolder {
        TextView idView;
        TextView priorityView;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.issue_list_item, null);
            ViewHolder viewHolder = new ViewHolder();
            {
                viewHolder.idView =       (TextView) view.findViewById(R.id.issue_id);
                viewHolder.priorityView = (TextView) view.findViewById(R.id.priority);
            }
            view.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) view.getTag();
        {
            if (isResolved(position)) {
                holder.idView.setPaintFlags(holder.idView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.idView.setEnabled(false);
            } else {
                holder.idView.setPaintFlags(holder.idView.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
                holder.idView.setEnabled(true);
            }

            holder.idView.setText(String.valueOf(getIssueId(position)));

            String priority = getIssueField(position, RESTyoutrack.FIELD_ISSUE_PRIORITY);

            if (priority != null) {
                try {
                    switch (RESTyoutrack.PRIORITY_VALUES.valueOf(priority)) {
                        case Minor :
                            holder.priorityView.setText("M");
                            holder.priorityView.setTextColor(resources.getColor(R.color.priority_minor));
                            holder.priorityView.setBackgroundColor(resources.getColor(R.color.priority_background_minor));
                            break;
                        case Major :
                            holder.priorityView.setText("M");
                            holder.priorityView.setTextColor(resources.getColor(R.color.priority_major));
                            holder.priorityView.setBackgroundColor(resources.getColor(R.color.priority_background_major));
                            break;
                        case Critical :
                            holder.priorityView.setText("C");
                            holder.priorityView.setTextColor(resources.getColor(R.color.priority_critical));
                            holder.priorityView.setBackgroundColor(resources.getColor(R.color.priority_background_critical));
                            break;
                        case Showstopper :
                            holder.priorityView.setText("S");
                            holder.priorityView.setTextColor(resources.getColor(R.color.priority_showStopper));
                            holder.priorityView.setBackgroundColor(resources.getColor(R.color.priority_background_showStopper));
                            break;
                        default :
                            holder.priorityView.setText("N");
                            holder.priorityView.setTextColor(resources.getColor(R.color.priority_normal));
                            holder.priorityView.setBackgroundColor(resources.getColor(R.color.priority_background_normal));
                            break;
                    }
                } catch (IllegalArgumentException ex) {
                }
            } else {
                holder.priorityView.setText("N");
                holder.priorityView.setTextColor(resources.getColor(R.color.priority_normal));
                holder.priorityView.setBackgroundColor(resources.getColor(R.color.priority_background_normal));
            }

        }
        return view;
    }

    public boolean isResolved(int position) {
        String state = getIssueField(position, RESTyoutrack.FIELD_ISSUE_STATE);
        return state != null && statesEqualsState(state);
    }
    private boolean statesEqualsState(String state){
        for (int i = 0; i < states.length; i++) {

            if (states[i].equalsIgnoreCase(state)) {
                return true;
            }
        }
        return false;
    }

    public String getIssueId(int position) {
        return getIssueField(position, RESTyoutrack.FIELD_ISSUE_ID);
    }

    @Override
    public Map<String, Object> getItem(int position) {
        return (Map<String, Object>) items.issues().get(position);
    }

    public<T> T getIssueField(int position, String fieldName) {
        Map<String, Object> item = getItem(position);
        Object field = null;
        if (item != null) {
            field = item.get(fieldName);
        }
        return (T) field;
    }
}
